# Install Dashboard to Kubernetes cluster

kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml

kubectl --kubeconfig="$DOKC" apply -f dashboard-adminuser.yml
kubectl --kubeconfig="$DOKC" apply -f dashboard-clusterrolebinding.yml

kubectl -n kube-system describe secret $(kubectl --kubeconfig="$DOKC" -n kube-system get secret | grep admin-user | awk '{print $1}') --kubeconfig="$DOKC"

eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLTR0d2Z4Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI1YzYzODcwZS01NDlkLTExZTktYWY5Yi01MjE1ZjgzYzI4NjMiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06YWRtaW4tdXNlciJ9.EA0B2n6P-2yFqi22zlpTDximt4semc5lp8xrY6G0yv_4yjyn0l90bKt-RUT9O7h5D-PM4YPnfzQuHljiccbazloRI-y5mYuG2sxc3vgpzoZzgqnFtLDE8OfhqaXcNjmVmljLe9sBgBha8oi5I5dAIGJcCuAVUGvi0Wf8KoX64q7UC14M9mlU64PDznXJxRgItZrCCIM2mH-EqVV8ljwanpj0vqKgymd1dLX28ea6MFu6R5PY0v96vBuOep7fx4TgmgIXj7YV2qfF4rrdmPwypNnr8KF7pgJxKE56hDnsRBswohM66OImOmnW7y5fLaGYUJvixybs4PK6LvLa7ZtREQ

kubectl --kubeconfig="$DOKC" proxy &

open https://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/

Select Token checkbox and paste the bearer token obtained from `describe secret` command
